<?php

namespace App\Repositories\Csv;


use App\Models\Csv;
use App\Repositories\EmployeeRepositoryInterface;

class EmployeeRepository implements EmployeeRepositoryInterface
{
    public function convertCsvToEmployeeList(string $csvPath) : Csv {
        $data = array_map('str_getcsv', file($csvPath));

        $serializedDataArray      = [];
        $columnNames              = [];
        $columnNamesWithoutSpaces = [];
        foreach ($data[0] as $columnName) {
            $columnNames[]              = $columnName;
            $columnNamesWithoutSpaces[] = str_replace(' ', '', $columnName);
        }

        unset($data[0]);
        foreach ($data as $userInfo) {
            $employee = new class{};
            foreach ($columnNamesWithoutSpaces as $index => $columnName) {
                $employee->$columnName = $userInfo[$index];
            }
            $serializedDataArray[] = $employee;
        }

        $csv = new Csv();
        $csv->setColumnNames($columnNames);
        $csv->setEmployeeList($serializedDataArray);

        return $csv;
    }
}
