<?php


namespace App\Repositories;


use App\Models\Csv;

interface EmployeeRepositoryInterface
{
    public function convertCsvToEmployeeList(string $csvPath) : Csv ;
}
