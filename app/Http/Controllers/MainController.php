<?php

namespace App\Http\Controllers;


use App\MatchAlgorithms\MatchBy3ColumnsAlgorithm;
use App\Http\Requests\UploadCsvRequest;
use App\MatchAlgorithms\MatchByLocationPreferenceAlgorithm;
use App\Models\MatchData;
use App\Repositories\EmployeeRepositoryInterface;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class MainController extends BaseController
{
    use ValidatesRequests;

    private EmployeeRepositoryInterface $employeeRepository;

    public function __construct(EmployeeRepositoryInterface $employeeRepository)
    {
        $this->employeeRepository = $employeeRepository;
    }

    function uploadCsv()
    {
        return view('upload_csv', []);
    }

    function uploadCsvPost(UploadCsvRequest $request)
    {
        $path = $request->file('employeeList')->getRealPath();

        $csv = $this->employeeRepository->convertCsvToEmployeeList($path);

//        $algorithm = new MatchBy3ColumnsAlgorithm();
        $algorithm = new MatchByLocationPreferenceAlgorithm();

        $matchData = $algorithm->getMatchedUsers($csv->getEmployeeList());


        return view('employee_matches', ['matchData' => $matchData, 'columnNames' => $csv->getColumnNames()]);
    }
}
