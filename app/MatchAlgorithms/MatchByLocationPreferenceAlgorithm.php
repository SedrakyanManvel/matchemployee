<?php


namespace App\MatchAlgorithms;

class MatchByLocationPreferenceAlgorithm extends MatchBy3ColumnsAlgorithm
{
    protected function calculateMatchPercent($employee1, $employee2) : int {
        $percent = 0;

        if($this->checkLocationPreferenceMatch($employee1, $employee2))
            $percent = parent::calculateMatchPercent($employee1, $employee2);

        return $percent;
    }

    private function checkLocationPreferenceMatch($employee1, $employee2) : bool {
        $isSameLocation = $employee1->Location == $employee2->Location;

        if($isSameLocation) {
            if(strtolower($employee1->SameLocationPreference) !== 'no'
            && strtolower($employee2->SameLocationPreference) !== 'no')
                return true;
        } else {
            if(strtolower($employee1->SameLocationPreference) !== 'yes'
                && strtolower($employee2->SameLocationPreference) !== 'yes')
                return true;
        }

        return false;
    }
}
