<?php

namespace App\MatchAlgorithms;

use App\Models\MatchData;

interface Algorithm
{
    function getMatchedUsers(array $userList) : MatchData;
}
