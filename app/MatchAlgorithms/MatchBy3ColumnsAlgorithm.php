<?php


namespace App\MatchAlgorithms;


use App\Models\EmployeeMatchData;
use App\Models\MatchData;

class MatchBy3ColumnsAlgorithm implements Algorithm
{
    protected array $matrix;
    protected array $employeeList;

    function getMatchedUsers(array $employeeList): MatchData
    {
        $this->employeeList = $employeeList;
        $this->createMatrix($employeeList);
        $indexRange = range(0, count($this->employeeList)-1);
        $indexFactorial = gmp_fact(count($this->employeeList)*10);
        ini_set('xdebug.max_nesting_level', $indexFactorial);
        $permutations = $this->getUserIndexPermutations($indexRange);
        return $this->getBestMatch($permutations,$indexRange);
    }


    protected function createMatrix(array $employeeList)
    {
        $this->matrix = [];

        foreach ($employeeList as $index => $employee1) {
            foreach ($employeeList as $index2 => $employee2) {
                $percent = $this->calculateMatchPercent($employee1, $employee2);

                $matchData = new EmployeeMatchData($employee1, $employee2, $percent);
                $this->matrix[$index][$index2] = $matchData;
            }
        }
    }

    protected function calculateMatchPercent($employee1, $employee2) : int {
        $percent = 0;
        if($employee1->Email == $employee2->Email)
            return 0;

        if ($employee1->Division == $employee2->Division) {
            $percent += 30;
        }

        if (abs($employee1->Age - $employee2->Age) < 5) {
            $percent += 30;
        }

        if ($employee1->Timezone == $employee2->Timezone) {
            $percent += 40;
        }

        return $percent;
    }

    protected function getUserIndexPermutations($indexRange) : array{

        if(count($indexRange)==1)
            return [$indexRange];

        $availableMethods = [];

        foreach ($indexRange as $key => $userData) {

            $newArr = $indexRange;

            unset($newArr[$key]);

            $arr = $this->getUserIndexPermutations($newArr);
            foreach ($arr as $nestedArr) {
                array_unshift($nestedArr, $userData);
                $availableMethods[] = $nestedArr;
            }
        }
        return $availableMethods;
    }

    protected function getBestMatch(array $permutations, array $indexRange) : MatchData {
        $sum =0;

        $matchData = new MatchData();

        foreach ($permutations as $randArr) {
            $sumForArr =0;
            $userListForArr =[];
            $uncheckedUsers = $indexRange;
            for ($i = 0; $i < count($this->employeeList); $i++) {
                if($indexRange[$i] == $randArr[$i])
                    break;
                if(!in_array($indexRange[$i], $uncheckedUsers) || !in_array($randArr[$i], $uncheckedUsers))
                    continue;

                unset($uncheckedUsers[$indexRange[$i]]);
                unset($uncheckedUsers[$randArr[$i]]);

                $sumForArr += $this->matrix[$indexRange[$i]][$randArr[$i]]->getPercent();
                $userListForArr[] = $this->matrix[$indexRange[$i]][$randArr[$i]];
            }
            if($sumForArr > $sum)
            {
                $sum = $sumForArr;
                $matchData->setMatchedUsers($userListForArr);
            }
        }

        $averagePercent = ($sum <> 0) ? $sum/count($matchData->getMatchedUsers()) : 0;

        $matchData->setAveragePercent($averagePercent);

        return $matchData;
    }
}
