<?php

namespace App\Models;

class EmployeeMatchData
{
    private $firstEmployee;
    private $secondEmployee;
    private int $percent;

    public function __construct($firstEmployee, $secondEmployee, int $percent)
    {
        $this->firstEmployee = $firstEmployee;
        $this->secondEmployee = $secondEmployee;
        $this->percent = $percent;
    }

    public function getFirstEmployee()
    {
        return $this->firstEmployee;
    }

    public function setFirstEmployee($firstEmployee): void
    {
        $this->firstEmployee = $firstEmployee;
    }

    public function getSecondEmployee()
    {
        return $this->secondEmployee;
    }

    public function setSecondEmployee($secondEmployee): void
    {
        $this->secondEmployee = $secondEmployee;
    }

    /**
     * @return int
     */
    public function getPercent(): int
    {
        return $this->percent;
    }

    /**
     * @param int $percent
     */
    public function setPercent(int $percent): void
    {
        $this->percent = $percent;
    }
}
