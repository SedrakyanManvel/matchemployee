<?php


namespace App\Models;


class MatchData
{
    private float $averagePercent = 0;
    private array $matchedUsers = [];

    /**
     * @return float
     */
    public function getAveragePercent(): float
    {
        return $this->averagePercent;
    }

    /**
     * @param float $averagePercent
     */
    public function setAveragePercent(float $averagePercent): void
    {
        $this->averagePercent = $averagePercent;
    }

    /**
     * @return array
     */
    public function getMatchedUsers(): array
    {
        return $this->matchedUsers;
    }

    /**
     * @param array $matchedUsers
     */
    public function setMatchedUsers(array $matchedUsers): void
    {
        $this->matchedUsers = $matchedUsers;
    }
}
