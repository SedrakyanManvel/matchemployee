<?php

namespace App\Models;


class Csv
{
    private array $employeeList = [];
    private array $columnNames = [];

    /**
     * @return array
     */
    public function getEmployeeList(): array
    {
        return $this->employeeList;
    }

    /**
     * @param array $employeeList
     */
    public function setEmployeeList(array $employeeList): void
    {
        $this->employeeList = $employeeList;
    }

    /**
     * @return array
     */
    public function getColumnNames(): array
    {
        return $this->columnNames;
    }

    /**
     * @param array $columnNames
     */
    public function setColumnNames(array $columnNames): void
    {
        $this->columnNames = $columnNames;
    }
}
