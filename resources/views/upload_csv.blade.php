@extends('layouts.app')
@section('content')
    <div class="row h-100 justify-content-center align-items-center">
        <div class="col-10 col-md-8 col-lg-6">
            <form action="{{route('uploadCsvPost')}}" method="POST" enctype="multipart/form-data">
                <p class="description">Choose employee list for getting best matches</p>
                @csrf
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <div class="custom-file" style="margin-bottom: 10px">
                    <input name="employeeList" accept=".csv,.xls,.xlsx" type="file" class="custom-file-input"
                           id="exampleFormControlFile1">
                    <label class="custom-file-label" for="exampleFormControlFile1">Choose file</label>
                </div>
                <button type="submit" class="btn btn-primary btn-customized">Upload CSV</button>
            </form>
        </div>
    </div>
@endsection
