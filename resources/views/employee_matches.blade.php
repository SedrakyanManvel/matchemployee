@extends('layouts.app')
@section('content')

    <h1>Matched Users</h1>
    <h2>Average match is {{$matchData->getAveragePercent()}}%</h2>
    <table class="table">
        <thead>
        <tr>
            <th scope="col">Match Percent</th>
            @foreach($columnNames as $column)
                <th>{{$column}}</th>
            @endforeach
        </tr>
        </thead>
        <tbody>
        @foreach($matchData->getMatchedUsers() as $match)
            <tr>
                <th rowspan="2" scope="row">{{$match->getPercent()}}</th>
                @foreach($match->getFirstEmployee() as $value)
                    <td>{{$value}}</td>
                @endforeach

            </tr>
            <tr>
                @foreach($match->getSecondEmployee() as $value)
                    <td>{{$value}}</td>
                @endforeach
            </tr>
        @endforeach
        </tbody>
    </table>
@endsection
